package com.benchmarksolution.detectringiot;

import android.content.Context;
import android.widget.Toast;

import java.util.Date;

/**
 * Created by Admin on 014 14-07-2017.
 */

public class CallReceiver extends PhonecallReceiver {



    @Override
    protected void onIncomingCallStarted(Context ctx, String number, Date start) {
        super.onIncomingCallStarted(ctx, number, start);
        simplyLogIt("OnIncomingCallStarted"+number+" "+start);
        makeToast(ctx,"OnIncomingCallStarted"+number+" "+start);

    }

    @Override
    protected void onOutgoingCallStarted(Context ctx, String number, Date start) {
        super.onOutgoingCallStarted(ctx, number, start);
        simplyLogIt("onOutgoingCallStarted"+number+" "+start);
        makeToast(ctx,"onOutgoingCallStarted"+number+" "+start);
    }

    @Override
    protected void onIncomingCallEnded(Context ctx, String number, Date start, Date end) {
        super.onIncomingCallEnded(ctx, number, start, end);
        simplyLogIt("onIncomingCallEnded"+number+" "+start);
        makeToast(ctx,"onIncomingCallEnded"+number+" "+start);
    }

    @Override
    protected void onOutgoingCallEnded(Context ctx, String number, Date start, Date end) {
        super.onOutgoingCallEnded(ctx, number, start, end);
        simplyLogIt("onOutgoingCallEnded"+number+" "+start);
        makeToast(ctx,"onOutgoingCallEnded"+number+" "+start);
    }

    @Override
    protected void onMissedCall(Context ctx, String number, Date start) {
        super.onMissedCall(ctx, number, start);
        simplyLogIt("onMissedCall"+number+" "+start);
        makeToast(ctx,"onMissedCall"+number+" "+start);
    }

    public void simplyLogIt(String string){
        System.out.println("DetectRingIoT:"+string);
    }
    void makeToast(Context ctx,String msg){
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }
}
