package com.benchmarksolution.detectringiot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button bstart,bstop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bstart = (Button)findViewById(R.id.btnstartservice);
        bstop = (Button)findViewById(R.id.btnstopservice);

        bstart.setOnClickListener(this);
        bstop.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnstartservice:
                startService(new Intent(this, CallDetectService.class));
                makeToast("Call Detect Service is started");
                break;
            case R.id.btnstopservice:
                stopService(new Intent(this, CallDetectService.class));
                makeToast("Call detection is now Stopped");
                break;
            default:
                break;

        }
    }

    void makeToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
